#!/bin/bash
VPN_ID=`nmcli -t c show | grep '^[^:]*:[^:]*:vpn:[^:]*$' | head -n 1 | cut -d : -f 1`
if [ -z "$VPN_ID" ] ; then
  kcmshell5 networkmanagement
  exit $?
fi
if nmcli -t c show "$VPN_ID" | grep -q '^GENERAL.STATE:activated$' ; then
  nmcli c down "$VPN_ID"
else
  nmcli c up "$VPN_ID"
fi
