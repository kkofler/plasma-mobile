# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-mobile package.
#
# Steve Allewell <steve.allewell@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-mobile\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-20 00:48+0000\n"
"PO-Revision-Date: 2022-12-31 17:48+0000\n"
"Last-Translator: Steve Allewell <steve.allewell@gmail.com>\n"
"Language-Team: British English <kde-i18n-doc@kde.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.3\n"

#: contents/ui/main.qml:12
#, kde-format
msgid "Virtual Keyboard"
msgstr "Virtual Keyboard"

#: contents/ui/main.qml:14
#, kde-format
msgid "On"
msgstr "On"

#: contents/ui/main.qml:15
#, kde-format
msgid "Off"
msgstr "Off"

#: contents/ui/main.qml:15
#, kde-format
msgid "Tap to open settings"
msgstr "Tap to open settings"
